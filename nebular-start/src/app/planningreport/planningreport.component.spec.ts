import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanningreportComponent } from './planningreport.component';

describe('PlanningreportComponent', () => {
  let component: PlanningreportComponent;
  let fixture: ComponentFixture<PlanningreportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlanningreportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanningreportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
