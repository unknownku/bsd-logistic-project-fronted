import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlanningreportRoutingModule } from './planningreport-routing.module';
import { PlanningreportComponent } from './planningreport.component';


@NgModule({
  declarations: [PlanningreportComponent],
  imports: [
    CommonModule,
    PlanningreportRoutingModule
  ]
})
export class PlanningreportModule { }
