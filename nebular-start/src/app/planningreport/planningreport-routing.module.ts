import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PlanningreportComponent } from './planningreport.component';

const routes: Routes = [{ path: '', component: PlanningreportComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlanningreportRoutingModule { }
