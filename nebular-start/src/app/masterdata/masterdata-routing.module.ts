import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MasterdataComponent } from './masterdata.component';

const routes: Routes = [{ path: '', component: MasterdataComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MasterdataRoutingModule { }
