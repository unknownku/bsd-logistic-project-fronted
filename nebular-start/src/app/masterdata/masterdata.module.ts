import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MasterdataRoutingModule } from './masterdata-routing.module';
import { MasterdataComponent } from './masterdata.component';


@NgModule({
  declarations: [MasterdataComponent],
  imports: [
    CommonModule,
    MasterdataRoutingModule
  ]
})
export class MasterdataModule { }
