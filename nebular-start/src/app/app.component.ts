import { Component } from '@angular/core';
import { NbMenuItem, NbSidebarService } from '@nebular/theme';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})


export class AppComponent {
  items: NbMenuItem[] = [
    {
      title: 'แผนการบรรทุกสินค้าและแผนการจัดส่ง',
      icon: 'book-open-outline',
      link: '/planningreport',
      home: true
    },
    {
      title: 'ข้อมูลพื้นฐาน',
      icon: 'hard-drive-outline',
      expanded: true,
      children: [
        {
          title: 'ข้อมูลยานพาหนะ',
          icon: 'car-outline',
          link: '/vehicles'
        },
        {
          title: 'ข้อมูลสินค้า',
          icon: 'shopping-bag-outline',
          link: '/products'
        },
        {
          title: 'ตั้งค่าพื้นฐาน',
          icon: 'settings-outline',
          link: '/settings'
        }
      ],
    },
    {
      title: 'ข้อมูลรายการสั่งซื้อ',
      icon: 'file-text-outline',
      link: '/purchaseorder'
    },
    {
      title: 'ข้อมูลการจัดส่ง',
      icon: 'car-outline',
      link: '/deliveryreport'
    }
  ];

  constructor(private readonly sidebarService: NbSidebarService) {
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle();
    return false;
  }
}
