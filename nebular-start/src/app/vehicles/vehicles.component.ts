import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';


@Component({
  selector: 'app-vehicles',
  templateUrl: './vehicles.component.html',
  styleUrls: ['./vehicles.component.scss']
})
export class VehiclesComponent implements OnInit {
  selectedItem = '1';
  inputItemNgModel;
  textareaItemNgModel;
  selectedVehicleFormControl = new FormControl();
  selectedItemFormControl = new FormControl();

  constructor() { }

  ngOnInit(): void {
  }

}
