import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DeliveryreportRoutingModule } from './deliveryreport-routing.module';
import { DeliveryreportComponent } from './deliveryreport.component';


@NgModule({
  declarations: [DeliveryreportComponent],
  imports: [
    CommonModule,
    DeliveryreportRoutingModule
  ]
})
export class DeliveryreportModule { }
