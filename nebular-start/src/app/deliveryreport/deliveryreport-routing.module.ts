import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DeliveryreportComponent } from './deliveryreport.component';

const routes: Routes = [{ path: '', component: DeliveryreportComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeliveryreportRoutingModule { }
