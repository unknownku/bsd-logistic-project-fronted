import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliveryreportComponent } from './deliveryreport.component';

describe('DeliveryreportComponent', () => {
  let component: DeliveryreportComponent;
  let fixture: ComponentFixture<DeliveryreportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeliveryreportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliveryreportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
